package pt.com.arquitetura.gateway.arquiteturagatewayserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class ArquiteturaGatewayServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArquiteturaGatewayServerApplication.class, args);
	}


	/**@Bean
	public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {

		RouteLocator  build =  builder.routes()
				.route("delivery-service-api", r -> r
						.path("/api/orders/**")
						.uri("lb://delivery-service/")
				)
				.route("provider-service-api", r -> r
						.path("/api/providers/**")
						.filters(f -> f.stripPrefix(1))
						.uri("lb://provider-service/")
				)
				.build();

		return build;
	}*/

}
