package pt.com.arquitetura.delivery.exception;

public class ProviderNotExistException extends   Exception {
    public ProviderNotExistException (String message){
        super(message);
    }

    public ProviderNotExistException (){
        super("Provider Not Exist");
    }
}
