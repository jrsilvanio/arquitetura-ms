package pt.com.arquitetura.delivery.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "provider-service",path = "/api/providers")
public interface ProviderClient {

    @RequestMapping(method = RequestMethod.GET, value = "/{id}", consumes = "application/json")
    Object findById(@PathVariable("id") Long id);

    @RequestMapping(value = "exist/{id}", method = RequestMethod.GET, consumes = "application/json")
    public Boolean isExist(@PathVariable("id") Long id);

}